package com.butilov.asbankserver.repository;

import com.butilov.asbankserver.entity.Credit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CreditsRepository extends CrudRepository<Credit, Long> {

}