package com.butilov.asbankserver.service;

import com.butilov.asbankserver.entity.Bill;
import com.butilov.asbankserver.repository.BillsRepository;
import org.springframework.stereotype.Service;

/**
 * Created by Dmitry Butilov
 * on 12.06.18.
 */
@Service
public class BillService {

    private BillsRepository billsRepository;

    public BillService(BillsRepository billsRepository) {
        this.billsRepository = billsRepository;
    }

    public void saveBill(Bill bill) {
        billsRepository.save(bill);
    }

    public Bill createBillWithName(String name) {
        Bill bill = new Bill();
        bill.setValue(0);
        bill.setName(name + "'s Bill");
        return billsRepository.save(bill);
    }

    public Bill findByName(Bill bill) {
        return billsRepository.findByName(bill.getName());
    }
}