package com.butilov.asbankserver.service;

import com.butilov.asbankserver.entity.BankUser;
import com.butilov.asbankserver.entity.Bill;
import com.butilov.asbankserver.entity.Transaction;
import com.butilov.asbankserver.repository.BankUsersRepository;
import com.butilov.asbankserver.repository.BillsRepository;
import com.butilov.asbankserver.repository.TransactionsRepository;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Calendar;

/**
 * Created by Dmitry Butilov
 * on 12.06.18.
 */
@Service
public class TransactionService {

    private BankUsersRepository usersRepository;
    private BillsRepository billsRepository;
    private TransactionsRepository transactionsRepository;

    public TransactionService(BankUsersRepository usersRepository, BillsRepository billsRepository, TransactionsRepository transactionsRepository) {
        this.usersRepository = usersRepository;
        this.billsRepository = billsRepository;
        this.transactionsRepository = transactionsRepository;
    }

    public void saveTransaction(Transaction transaction) {
        @NotNull BankUser user = transaction.getUser();
        BankUser foundUser = usersRepository.findByLogin(user.getLogin());
        if (foundUser != null) {
            Bill srcBill = foundUser.getBill();
            Bill foundSrcBill = billsRepository.findByName(srcBill.getName());
            Bill foundDestBill = billsRepository.findByName(transaction.getDestBill().getName());
            if (foundSrcBill != null && foundDestBill != null) {
                @NotNull long srcBillValue = foundSrcBill.getValue();
                @NotNull long transactionValue = transaction.getValue();
                if (srcBillValue >= transactionValue && transactionValue != 0) {
                    foundSrcBill.setValue(srcBillValue - transactionValue);
                    foundDestBill.setValue(foundDestBill.getValue() + transactionValue);
                    billsRepository.save(foundSrcBill);
                    billsRepository.save(foundDestBill);
                    transaction.setDestBill(foundDestBill);
                    transaction.setTime(Calendar.getInstance().getTimeInMillis());
                    transaction.setUser(foundUser);
                    transactionsRepository.save(transaction);
                    foundUser.getTransactions().add(transaction);
                    usersRepository.save(foundUser);
                }
            }
        }
    }
}